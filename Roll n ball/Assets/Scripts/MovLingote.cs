using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovLingote : MonoBehaviour
{
    bool tengoQueBajar = false;
    int rapidez = 10;
    void Update()
    {
        if (transform.position.y >= 22)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 18)
        {
            tengoQueBajar = false;
        }
        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }
    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }
    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
