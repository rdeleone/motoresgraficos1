﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagramaDeClases
{
    abstract public class Pieza
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private bool estado;

        public bool Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        public abstract void Mover();
        public virtual void Comer()
        {

        }
    }
}