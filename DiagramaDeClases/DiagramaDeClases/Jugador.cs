﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiagramaDeClases
{
    public class Jugador
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public void SeleccionarPieza()
        {

        }

        public void MoverPieza()
        {

        }

        public void Jaque()
        {

        }

        public void JaqueMate()
        {

        }
    }
}