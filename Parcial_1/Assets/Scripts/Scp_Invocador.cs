using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_Invocador : MonoBehaviour
{
    public GameObject invocacion;
    
    int contenemigo = 0;

    void Start()
    {
        StartCoroutine(InvocarEnemigo());
    }
    

    public void Invocar()
    {
        GameObject inv;
        Ray ray = new Ray(transform.position, transform.forward);

        inv = Instantiate(invocacion, ray.origin, transform.rotation);

    }

    IEnumerator InvocarEnemigo()
    {
        while (contenemigo < 5)
        {
            Invocar();
            yield return new WaitForSeconds(2f);
            contenemigo++;
        }
    }

}
