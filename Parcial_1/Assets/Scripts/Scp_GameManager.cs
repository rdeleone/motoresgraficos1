using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Scp_GameManager : MonoBehaviour
{
    public GameObject jugador;

    public TMPro.TMP_Text Cronometro;
    public TMPro.TMP_Text Estado;
    public TMPro.TMP_Text Pausa;
    
    float tiempoRestante;
    
    void Start()
    {
        
        ComenzarJuego();
        InicializarTextos();
        Estado.text = "";
    }

    void Update()
    {

        if (tiempoRestante == 0)
        {
            ComenzarJuego();
        }
        Reiniciar();
         

    }



    //Metodos personalizados
    #region
    private void ComenzarJuego()
    {
        jugador.transform.position = new Vector3(0f, 2f, -19f);
        
        StartCoroutine(ComenzarCronometro(60));
        
    }

    private IEnumerator ComenzarCronometro(float valorCronometro)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            yield return new WaitForSeconds(1.0f);
            Debug.Log(valorCronometro);
            
            Cronometro.text = "Tiempo restante: " + tiempoRestante--.ToString();
        }
        if (tiempoRestante <= 0)
        {
            Time.timeScale = 0;
            Estado.text = "�Game Over!";
            
        }

    }

    public void Reiniciar()
    {
        if (Input.GetKeyDown(KeyCode.R) || jugador.transform.position.y < -1)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }

    private void InicializarTextos()
    {
        Pausa.text = "Presiona 'R' para reiniciar.\nPara ganar agarra el zafiro.\nLos enemigos si te tocan, se reinicia la partida.";
    }

    #endregion


}
