using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_Explosion : MonoBehaviour
{

    private GameObject jugador;
    public int rapidez;
    int ditancia = 2;
    private Vector3 cabioEscala = new Vector3(1.5f, 1.5f, 1.5f);

    void Start()
    {
        jugador = GameObject.Find("Obj_Jugador");
    }

    
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        Explosion();
    }

    public void Explosion()
    {

        RaycastHit hit;

        Ray ray = new Ray(transform.position, transform.forward);

        if ((Physics.Raycast(ray, out hit) == true) && hit.distance < ditancia && hit.collider.name == "Obj_Jugador")
        {
            Debug.Log("El rayo tocó al objeto: " + hit.collider.name);
            StartCoroutine(AumentarTamaño());
        }
    }

    IEnumerator AumentarTamaño()
    {
        transform.localScale += cabioEscala;
        yield return new WaitForSeconds(1f);
        Destroy(GameObject.Find("Obj_Explocion"));
    }


}
