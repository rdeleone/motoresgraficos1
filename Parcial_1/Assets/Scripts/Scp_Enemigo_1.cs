using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_Enemigo_1 : MonoBehaviour
{
    public Camera camaraPrimeraPersona;
    public GameObject Obj_bala;
    private GameObject jugador;

    float spawnTime = 0.5f;
    float timeSinceSpawned = 0f;
    int ditancia = 15;

    private void Start()
    {
        jugador = GameObject.Find("Obj_Jugador");
    }

    void Update()
    {
        timeSinceSpawned += Time.deltaTime;
        transform.LookAt(jugador.transform);

        if (timeSinceSpawned >= spawnTime)
        {
            Raycast();
            timeSinceSpawned = 0;
        }
            

    }

    private void Raycast()
    {
        Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
          
        if ((Physics.Raycast(ray, out hit) == true) && hit.distance < ditancia && hit.collider.name == "Obj_Jugador")
        {
            Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            Disparo(ray);           
           
        }

    }

    public void Disparo(Ray ray)
    {
        GameObject pro;
        pro = Instantiate(Obj_bala, ray.origin, transform.rotation);

        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);

        Destroy(pro, 1.5f);
    }



}
