using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_Fusion : MonoBehaviour
{

    public GameObject fusion;
    public GameObject invocacion;
    private BoxCollider trigger;

    public int rapidez;


    void Start()
    {
        
    }

    
    void Update()
    {
        transform.LookAt(fusion.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Fusion") == true)
        {
            GameObject inv;
            inv = Instantiate(invocacion, transform.position, transform.rotation);
            collision.gameObject.SetActive(false);
        }
    }

}
