using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scp_Jugador : MonoBehaviour
{
    private Rigidbody rb;
    public CapsuleCollider col;
    public LayerMask capaPiso;
    private Vector3 cabioEscala = new Vector3(1.5f, 1.5f, 1.5f);
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text Estado;

    public float magnitudSalto;
    public float rapidez;
    private int contSaltos;
    private int cont;


    void Start()
    {
        cont = 0;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Estado.text = "";
        setearTextos();


    }

    void Update()
    {
        Saltar();

    }

    private void FixedUpdate()
    {
        Mover();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obj_tama�o"))
        {
            other.gameObject.SetActive(false);
            transform.localScale += cabioEscala;
        }
        if (other.gameObject.CompareTag("Obj_velocidad"))
        {
            other.gameObject.SetActive(false);
            rapidez += 3;
        }
        if (other.gameObject.CompareTag("Coleccion"))
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }


    }

    //Metodos personalizados
    #region

    private void Mover()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);

        rb.AddForce(vectorMovimiento * rapidez);
    }

    private void Saltar()
    {

        if (EstarEnPiso())
        {
            contSaltos = 0;
        }
        if (Input.GetKeyDown(KeyCode.Space) && (EstarEnPiso() || contSaltos < 1))
        {
            contSaltos++;
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
    }

    private bool EstarEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Recolectados: " + cont.ToString();
        if (cont >= 1)
        {
            Time.timeScale = 0;
            Estado.text = "�Ganaste!";
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.collider.CompareTag("bala")|| collision.collider.CompareTag("Enemigo"))
        {
            SceneManager.LoadScene("SampleScene");
        }
    }


    #endregion
}