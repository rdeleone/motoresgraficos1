using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_EnemigoPersecucion : MonoBehaviour
{

    private GameObject jugador;
    public int rapidez;

    
    void Start()
    {
        jugador = GameObject.Find("Obj_Jugador");
    }

    
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        

    }
}
