using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_hielo : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("fuego"))
        {
            this.desaparecer();
        }

    }
    private void desaparecer()
    {
        Destroy(gameObject);
    }

}
