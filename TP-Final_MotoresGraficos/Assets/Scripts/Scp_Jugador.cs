using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_Jugador : MonoBehaviour
{
    private Rigidbody rb;
    private int contSaltos;
    private GameObject Caja;
    private Rigidbody cajarb;
    public Transform objetivo;
    public Camera Camara;

    public CapsuleCollider col;
    public LayerMask capaPiso;
    public GameObject hielo;
    public GameObject proyectil;
    public GameObject msgPanel;
    public GameObject inputmsg;
    public GameObject msj1;
    public GameObject msj2;

    public float rapidez;
    public float magnitudSalto;
    public float VelDash;


    private bool salto = false;
    private bool Empuje = false;
    private bool dasher;
    private bool controlDash=false;
    private bool congelar = false;
    private bool tp = false;
    private bool fuego = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

        col = GetComponent<CapsuleCollider>();

        Caja = GameObject.Find("Caja");
        cajarb = Caja.GetComponent<Rigidbody>();
        cajarb.isKinematic = true;

        GestorDeAudio.instancia.ReproducirSonido("ambiente");

        msgPanel.SetActive(false);
        inputmsg.SetActive(false);
    }

    
    void Update()
    {

        Saltar();
        if (Empuje)
        {
            Empujar();
        }
        else
        {
            cajarb.isKinematic = true;
        }
        if (Input.GetKeyDown(KeyCode.Space)&& controlDash)
        {
            dasher = true;
        }
        

    }

    private void FixedUpdate()
    {
        Mover();
        if (dasher)
        {
            Dash();
        }

        if (tp && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true))
            {
                Teleportar();
                //Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            }
            tp = false;
        }

        if (fuego && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();

            rb.AddForce(Camara.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 2);
            fuego = false;
        }


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cartel"))
        {
            msgPanel.SetActive(true);
            inputmsg.SetActive(true);
            Time.timeScale = 0;
        }

    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cartel"))
        {
            msgPanel.SetActive(false);
            inputmsg.SetActive(false);
            Time.timeScale = 1;
            msj1.SetActive(false);
            msj2.SetActive(false);

        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Item_salto"))
        {
            //other.gameObject.SetActive(false);
            salto = true;
            controlDash = false;
            Empuje = false;
            congelar = false;
            tp = false;
            fuego = false;
        }

        if (other.gameObject.CompareTag("Item_empuje"))
        {
            //other.gameObject.SetActive(false);
            Empuje = true;
            salto = false;
            controlDash = false;
            congelar = false;
            tp = false;
            fuego = false;
            Debug.Log("item empuje obtenido");
        }

        if (other.gameObject.CompareTag("Item_dash"))
        {
            //other.gameObject.SetActive(false);
            
            controlDash = true;
            Empuje = false;
            salto = false;
            congelar = false;
            tp = false;
            fuego = false;
            Debug.Log("item dash obtenido");
        }

        if (other.gameObject.CompareTag("Item_frozen"))
        {
            congelar = true;
            //other.gameObject.SetActive(false);
            controlDash = false;
            Empuje = false;
            salto = false;
            tp = false;
            fuego = false;
        }
        if (other.gameObject.CompareTag("Agua") && congelar)
        {
            Congelar();
        }
        if (other.gameObject.CompareTag("Placatp"))
        {
            tp = true;
            congelar = false;
            controlDash = false;
            Empuje = false;
            salto = false;
            fuego = false;
        }
        if (other.gameObject.CompareTag("item_fuego"))
        {
            fuego = true;
            tp = false;
            congelar = false;
            controlDash = false;
            Empuje = false;
            salto = false;
        }


    }


    #region

    private void Mover()
    {
        GestorDeAudio.instancia.ReproducirSonido("pasos");
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidez;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidez;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }

    private void Empujar()
    {
        cajarb.isKinematic = false;
        cajarb.detectCollisions = true;
     
    }

    private void Saltar()
    {

        if (EstarEnPiso())
        {
            contSaltos = 0;
        }
        if (Input.GetKeyDown(KeyCode.Space) && (EstarEnPiso() || contSaltos < 1) && salto == true)
        {
            contSaltos++;
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);

        }


    }

    private bool EstarEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    private void Dash()
    {
        GestorDeAudio.instancia.ReproducirSonido("dash");
        rb.AddForce(transform.forward * VelDash, ForceMode.Impulse);
        dasher = false;
    }

    private void Congelar()
    {
        GameObject Hielo;
        float piesjugador = transform.position.y - transform.localScale.y;
        Hielo = Instantiate(hielo,new Vector3(transform.position.x,piesjugador,transform.position.z), transform.rotation);
    }


    private void Teleportar()
    {

    this.transform.position = objetivo.transform.position;

            
    }

    #endregion
}
