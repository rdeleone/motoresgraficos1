using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_cartel : MonoBehaviour
{

    private string input;
    public GameObject msgPanel1;
    public GameObject msgPanel2;

    void Start()
    {
        msgPanel1.SetActive(false);
        msgPanel2.SetActive(false);
    }
   

    public void ReadStringInput(string s)
    {
        input = s;
        if (s == "pass")    
        {
            msgPanel1.SetActive(true);
            Time.timeScale = 1;
        }
        else
        {
            msgPanel2.SetActive(true);
        }
    }
}
