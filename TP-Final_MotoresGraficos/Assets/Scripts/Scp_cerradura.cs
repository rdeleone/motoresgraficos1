using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_cerradura : MonoBehaviour
{
    public GameObject msgPanel1;
    private GameObject llave = null;

    // Start is called before the first frame update
    void Start()
    {
        msgPanel1.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (llave == null)
        {
           
            msgPanel1.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cerradura"))
        {
            llave = other.gameObject;
            msgPanel1.SetActive(true);
        }
        else
        {
            llave = null;
        }
       
    }
}
