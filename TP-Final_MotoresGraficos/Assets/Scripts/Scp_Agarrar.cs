using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scp_Agarrar : MonoBehaviour
{
    public GameObject handpoint;
    private GameObject pickedObjet = null;
    public GameObject msgPanel;

    
    void Start()
    {
       msgPanel.SetActive(false);

    }


    void Update()
    {
        if (pickedObjet != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                pickedObjet.GetComponent<Rigidbody>().useGravity = true;
                pickedObjet.GetComponent<Rigidbody>().isKinematic = false;
                pickedObjet.gameObject.transform.SetParent(null);
                pickedObjet = null;
                msgPanel.SetActive(false);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Llave"))
        {
            if (Input.GetKeyDown(KeyCode.Space) && pickedObjet == null)
            {
                other.GetComponent<Rigidbody>().useGravity = false;
                other.GetComponent<Rigidbody>().isKinematic = true;
                other.transform.position = handpoint.transform.position;
                other.gameObject.transform.SetParent(handpoint.gameObject.transform);
                pickedObjet = other.gameObject;
                msgPanel.SetActive(true);
            }

        }
    }




}
